import { takeEvery, put, call } from 'redux-saga/effects';
import { fetchUser } from '../../routines/routines';
import * as userService from '../../services/userService';

function* userRequest(action) {
    try {
        yield put(fetchUser.request());

        const response = yield call(userService.getUser, action.payload.id);
        if (typeof(response) === 'string') {
            throw new Error(response);
        }

        yield put(fetchUser.success(response));
    } catch(error) {
        yield put(fetchUser.failure(error.message));
    } finally {
        yield put(fetchUser.fulfill());
    }
}

export default function* userSaga() {
    yield takeEvery(fetchUser.TRIGGER, userRequest);
}