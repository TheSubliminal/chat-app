import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchUser } from '../../routines/routines';
import {
    addUser,
    editUser
} from '../UserList/actions';
import {
    removeEditedUser
} from './actions';
import Spinner from '../../components/Spinner/Spinner';

import 'semantic-ui-form/form.css';
import Error from '../../components/Error/Error';

class UserEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            avatar: '',
            isValid: false
        };

        this.onChange = this.onChange.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.fetchUser({
                id: this.props.match.params.id
            });
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.editedUser.user && nextProps.match.params.id && (nextProps.editedUser.user.id !== prevState.userId)) {
            const {id, username, password, avatar } = nextProps.editedUser.user;
            return {
                userId: id,
                username,
                password,
                avatar
            };
        } else {
            return null;
        }
    }

    onChange(event) {
        const propName = event.target.name;

        this.setState({
            [propName]: event.target.value
        });

        this.validate();
    }

    onCancel() {
        this.props.history.push('/users');
    }

    onSubmit(event) {
        event.preventDefault();

        if (this.props.match.params.id) {
            this.props.editUser(this.state.userId, this.state.username.trim(), this.state.password.trim(), this.state.avatar.trim());
        } else {
            this.props.addUser(this.state.username.trim(), this.state.password.trim(), this.state.avatar.trim());
        }
        this.props.removeEditedUser();
        this.props.history.push('/users');
    }

    validate() {
        this.setState(state => ({
            isValid: !!state.username.trim() && !!state.password.trim() && !!state.avatar.trim()
        }));
    }

    render() {
        const { username, password, avatar, isValid } = this.state;
        const { editedUser } = this.props;
        const { loading, error } = editedUser;
        const headerText = this.props.match.params.id ? 'Edit User' : 'Add User';

        return (
            <div className="editor-container">
                {error && <Error message={error}/>}
                {loading
                    ? <Spinner/>
                    : (
                        <div className="editor wide">
                            <h2 className="editor-header">{headerText}</h2>
                            <div className="ui divider"/>
                            <form className="ui form" onSubmit={this.onSubmit}>
                                <div className="field">
                                    <label htmlFor="username">Username</label>
                                    <input type="text" id="username" name="username" value={username} onChange={this.onChange}/>
                                </div>
                                <div className="field">
                                    <label htmlFor="password">Password</label>
                                    <input type="password" id="password" name="password" value={password} onChange={this.onChange}/>
                                </div>
                                <div className="field">
                                    <label htmlFor="avatar">Avatar URL</label>
                                    <input type="text" id="avatar" name="avatar" value={avatar} onChange={this.onChange}/>
                                </div>
                                <div className="controls">
                                    <button className="ui primary button" type="submit" disabled={loading || !isValid}>
                                        Save
                                    </button>
                                    <button className="ui button" type="button" onClick={this.onCancel} disabled={loading}>
                                        Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    )}
            </div>
        );
    }
}

UserEditor.propTypes = {
    editedUser: PropTypes.exact({
        user: PropTypes.shape({
            id: PropTypes.number.isRequired,
            username: PropTypes.string.isRequired,
            password: PropTypes.string.isRequired,
            avatar: PropTypes.string
        }),
        loading: PropTypes.bool.isRequired,
        error: PropTypes.string
    }).isRequired,
    fetchUser: PropTypes.func.isRequired,
    addUser: PropTypes.func.isRequired,
    editUser: PropTypes.func.isRequired,
    removeEditedUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    editedUser: state.editedUser
});

const mapDispatchToProps = {
    fetchUser,
    addUser,
    editUser,
    removeEditedUser
};

export default connect(mapStateToProps, mapDispatchToProps)(UserEditor);