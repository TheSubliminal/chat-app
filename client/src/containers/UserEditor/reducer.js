import { fetchUser } from '../../routines/routines';
import { REMOVE_EDITED_USER } from './actionTypes';

const initialState = {
    user: null,
    loading: false,
    error: null
};

const editedUser = (state = initialState, action) => {
    switch (action.type) {
        case fetchUser.TRIGGER:
            return {
                ...state,
                loading: true
            };
        case fetchUser.SUCCESS:
            return {
                ...state,
                user: action.payload
            };
        case fetchUser.FAILURE:
            return {
                ...state,
                error: action.payload
            };
        case fetchUser.FULFILL:
            return {
                ...state,
                loading: false
            };
        case REMOVE_EDITED_USER:
            return {
                ...initialState
            };
        default:
            return state;
    }
};

export default editedUser;