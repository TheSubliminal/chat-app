import { REMOVE_EDITED_USER } from './actionTypes';

export const removeEditedUser = () => {
    return {
        type: REMOVE_EDITED_USER
    }
};