import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Chat from '../../containers/Chat/Chat';
import MessageEditor from '../MessageEditor/MessageEditor';
import LoginPage from '../LoginPage/LoginPage';
import UserList from '../UserList/UserList';
import UserEditor from '../UserEditor/UserEditor';
import PageHeader from '../../components/PageHeader/PageHeader';
import PrivateRoute from '../PrivateRoute/PrivateRoute';
import NotFound from '../../components/NotFound/NotFound';

function App() {
    return (
        <>
            <PageHeader />
            <main>
                <Switch>
                    <PrivateRoute exact path="/" component={Chat}/>
                    <PrivateRoute path="/message/:id" component={MessageEditor}/>
                    <PrivateRoute exact path="/message" component={MessageEditor}/>
                    <PrivateRoute exact path="/users" adminPath component={UserList}/>
                    <PrivateRoute path="/user/:id" adminPath component={UserEditor}/>
                    <PrivateRoute exact path="/user" adminPath component={UserEditor}/>
                    <Route path="/login" component={LoginPage}/>
                    <Route component={NotFound}/>
                </Switch>
            </main>
        </>
    );
}

export default App;