import {
    SET_CURRENT_USER,
    REMOVE_CURRENT_USER
} from './actionTypes';

const initialState = null;

const currentUser = (state = initialState, action) => {
    switch(action.type) {
        case SET_CURRENT_USER:
            return {
                ...action.payload.user
            };
        case REMOVE_CURRENT_USER:
            return null;
        default:
            return state;
    }
};

export default currentUser;