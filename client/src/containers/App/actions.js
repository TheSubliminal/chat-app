import {
    SET_CURRENT_USER,
    REMOVE_CURRENT_USER
} from './actionTypes';

export const setCurrentUser = (user) => {
    return {
        type: SET_CURRENT_USER,
        payload: {
            user
        }
    };
};

export const removeCurrentUser = () => {
    return {
        type: REMOVE_CURRENT_USER
    };
};