import {
    ADD_USER,
    EDIT_USER,
    DELETE_USER
} from './actionTypes';

export const addUser = (username, password, avatar) => {
    return {
        type: ADD_USER,
        payload: {
            username,
            password,
            avatar
        }
    };
};

export const editUser = (id, username, password, avatar) => {
    return {
        type: EDIT_USER,
        payload: {
            id,
            username,
            password,
            avatar
        }
    };
};

export const deleteUser = (id) => {
    return {
        type: DELETE_USER,
        payload: {
            id
        }
    };
};