import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import UserItem from '../../components/UserItem/UserItem';
import Spinner from '../../components/Spinner/Spinner';
import { fetchUsers } from '../../routines/routines';

import {
    deleteUser
} from './actions';

import './user-list.css';
import Error from '../../components/Error/Error';

class UserList extends React.Component {
    constructor(props) {
        super(props);

        this.onAdd = this.onAdd.bind(this);
        this.onEdit = this.onEdit.bind(this);
    }

    componentDidMount() {
        if (!this.props.userData.users.length) this.props.fetchUsers();
    }

    onAdd() {
        this.props.history.push('/user');
    }

    onEdit(id) {
        this.props.history.push(`/user/${id}`);
    }

    render() {
        const { userData, deleteUser } = this.props;
        const { users, loading, error } = userData;

        return(
            <div className="user-list-container">
                {error && <Error message={error}/>}
                {loading
                    ? <Spinner/>
                    :(
                        <div className="user-list">
                            {users.map(({ id, username, avatar }) => (
                                <UserItem
                                    onDelete={() => deleteUser(id)}
                                    onEdit={() => this.onEdit(id)}
                                    username={username}
                                    avatar={avatar}
                                    key={id}
                                />
                            ))}
                        </div>
                    )
                }
                <button className="huge primary ui button fixed" type="button" onClick={this.onAdd}>
                    Add User
                </button>
            </div>

        );
    }

}

UserList.propTypes = {
    userData: PropTypes.exact({
        users: PropTypes.arrayOf(
            PropTypes.exact({
                id: PropTypes.number.isRequired,
                username: PropTypes.string.isRequired,
                password: PropTypes.string.isRequired,
                avatar: PropTypes.string,
                isAdmin: PropTypes.bool.isRequired
            })
        ),
        loading: PropTypes.bool.isRequired,
        error: PropTypes.string
    }).isRequired,
    fetchUsers: PropTypes.func.isRequired,
    deleteUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    userData: state.userData
});

const mapDispatchToProps = {
    fetchUsers,
    deleteUser
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);