import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { auth } from '../../routines/routines';
import Error from '../../components/Error/Error';

import 'semantic-ui-form/form.css';
import 'semantic-ui-button/button.css';
import './login-page.css';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: ''
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(event) {
        const value = event.target.value;
        const propName = event.target.name;

        this.setState({
            [propName]: value
        });
    }

    onSubmit(event) {
        event.preventDefault();

        this.props.auth({
            username: this.state.username,
            password: this.state.password
        });
    }

    render() {
        const { currentUser, loginData: { loading, error } } = this.props;

        return(
            <div className="editor-container">
                {error && <Error message={error}/>}
                {currentUser && (currentUser.isAdmin ? <Redirect to="/users"/> : <Redirect to="/"/>)}
                <div className="editor">
                    <form className="login-form ui form" onSubmit={this.onSubmit}>
                        <input type="text" name="username" placeholder="Username" onChange={this.onChange}/>
                        <input type="password" name="password" placeholder="Password" onChange={this.onChange}/>
                        <button className="primary ui button" type="submit" disabled={loading}>Login</button>
                    </form>
                </div>
            </div>
        );
    }
}

LoginPage.propTypes ={
    currentUser: PropTypes.shape({
        isAdmin: PropTypes.bool.isRequired
    }),
    loginData: PropTypes.exact({
        data: PropTypes.shape({
            isAdmin: PropTypes.bool.isRequired
        }),
        loading: PropTypes.bool.isRequired,
        error: PropTypes.string
    }),
    auth: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    currentUser: state.currentUser,
    loginData: state.loginData
});

const mapDispatchToProps = {
    auth
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);