import React from 'react';
import PropTypes from 'prop-types';
import Spinner from '../../components/Spinner/Spinner';
import { removeEditedMessage } from './actions';
import { addMessage, editMessage } from '../Chat/actions';
import { fetchMessage } from '../../routines/routines';
import { connect } from 'react-redux';

import 'semantic-ui-form/form.css';
import 'semantic-ui-button/button.css';
import 'semantic-ui-divider/divider.css';
import Error from '../../components/Error/Error';

class MessageEditor extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            text: '',
            isValid: false
        };

        this.onChange = this.onChange.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.fetchMessage({
                id: this.props.match.params.id
            });
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.editedMessage.message && nextProps.match.params.id && (nextProps.editedMessage.message.id !== prevState.messageId)) {
            return {
                text: nextProps.editedMessage.message.message,
                messageId: nextProps.editedMessage.message.id
            };
        } else {
            return null;
        }
    }

    onChange(event) {
        this.setState({
            text: event.target.value
        });
        this.validate();
    }

    onCancel() {
        this.props.history.push('/');
    }

    onSubmit(event) {
        event.preventDefault();

        if (this.props.match.params.id) {
            this.props.editMessage(this.props.editedMessage.message.id, this.state.text.trim());
        } else {
            this.props.addMessage(this.props.currentUser.username, this.props.currentUser.avatar, this.state.text.trim());
        }
        this.props.removeEditedMessage();
        this.props.history.push('/');
    }

    validate() {
        this.setState(state => ({
            isValid: !!state.text.trim()
        }));
    }

    render() {
        const { text, isValid } = this.state;
        const { editedMessage } = this.props;
        const { loading, error } = editedMessage;
        const headerText = this.props.match.params.id ? 'Edit Message' : 'Add Message';

        return (
            <div className="editor-container">
                {error && <Error message={error}/>}
                {loading
                    ? <Spinner/>
                    : (
                        <div className="editor wide">
                            <h2 className="editor-header">{headerText}</h2>
                            <div className="ui divider"/>
                            <form className="ui form" onSubmit={this.onSubmit}>
                                <div className="field">
                                    <textarea name="message-text" value={text} onChange={this.onChange}/>
                                </div>
                                <div className="controls">
                                    <button className="ui primary button" type="submit" disabled={loading || !isValid}>
                                        Save
                                    </button>
                                    <button className="ui button" type="button" onClick={this.onCancel} disabled={loading}>
                                        Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    )}
            </div>
        );
    }
}

MessageEditor.propTypes = {
    currentUser: PropTypes.shape({
        username: PropTypes.string.isRequired,
        avatar: PropTypes.string
    }).isRequired,
    editedMessage: PropTypes.exact({
        message: PropTypes.shape({
            id: PropTypes.number.isRequired,
            message: PropTypes.string.isRequired
        }),
        loading: PropTypes.bool.isRequired,
        error: PropTypes.string
    }),
    fetchMessage: PropTypes.func.isRequired,
    addMessage: PropTypes.func.isRequired,
    editMessage: PropTypes.func.isRequired,
    removeEditedMessage: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    currentUser: state.currentUser,
    editedMessage: state.editedMessage
});

const mapDispatchToProps = {
    fetchMessage,
    addMessage,
    editMessage,
    removeEditedMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageEditor);