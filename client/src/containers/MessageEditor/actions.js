import { REMOVE_EDITED_MESSAGE } from './actionTypes';

export const removeEditedMessage = () => {
    return {
        type: REMOVE_EDITED_MESSAGE
    }
};