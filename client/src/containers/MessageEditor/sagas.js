import { takeEvery, put, call } from 'redux-saga/effects';
import { fetchMessage } from '../../routines/routines';
import * as messageService from '../../services/messageService';

function* messageRequest(action) {
    try {
        yield put(fetchMessage.request());

        const response = yield call(messageService.getMessage, action.payload.id);
        if (typeof(response) === 'string') {
            throw new Error(response);
        }

        yield put(fetchMessage.success(response));
    } catch(error) {
        yield put(fetchMessage.failure(error.message));
    } finally {
        yield put(fetchMessage.fulfill());
    }
}

export default function* messageSaga() {
    yield takeEvery(fetchMessage.TRIGGER, messageRequest);
}