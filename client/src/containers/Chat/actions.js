import {
    ADD_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE
} from './actionTypes';

export const addMessage = (user, avatar, message) => {
    return {
        type: ADD_MESSAGE,
        payload: {
            user,
            avatar,
            message
        }
    };
};

export const editMessage = (id, message) => {
    return {
        type: EDIT_MESSAGE,
        payload: {
            id,
            message
        }
    };
};

export const deleteMessage = (id) => {
    return {
        type: DELETE_MESSAGE,
        payload: {
            id
        }
    };
};