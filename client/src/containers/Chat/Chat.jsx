import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import memoize from 'memoize-one';
import ChatHeader from '../../components/ChatHeader/ChatHeader';
import MessageList from '../../components/MessageList/MessageList';
import MessageInput from '../../components/MessageInput/MessageInput';
import Spinner from '../../components/Spinner/Spinner';
import { fetchMessages } from '../../routines/routines';

import {
    addMessage,
    deleteMessage
} from './actions';

import './chat.css';
import 'semantic-ui-button/button.css';
import Error from '../../components/Error/Error';

class Chat extends React.Component {
    constructor(props) {
        super(props);

        this.messageSort = memoize((messages) => {
            return messages.sort((messageA, messageB) => {
                return new Date(messageA.created_at) - new Date(messageB.created_at);
            });
        });

        this.arrowUpHandler = this.arrowUpHandler.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onAddMessage = this.onAddMessage.bind(this);
    }

    componentDidMount() {
        if (!this.props.messageData.messages.length) this.props.fetchMessages();

        document.body.addEventListener('keydown', this.arrowUpHandler);
    }

    componentWillUnmount() {
        document.body.removeEventListener('keydown', this.arrowUpHandler);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.messageData.messages.length < this.props.messageData.messages.length) {
            const scrollingElement = (document.scrollingElement || document.body);
            scrollingElement.scrollTop = scrollingElement.scrollHeight;
        }
    }

    countUsers() {
        return new Set(this.props.messageData.messages.map(message => message.user)).size;
    }

    getLastMessageTime() {
        return this.props.messageData.messages
            .map(message => message.created_at)
            .reduce((acc, curr) => new Date(acc) > new Date(curr) ? acc : curr, '');
    }

    arrowUpHandler(event) {
        const { keyCode } = event;
        const lastMessage = this.props.messageData.messages[this.props.messageData.messages.length - 1];

        if (keyCode === 38 && lastMessage.user === this.props.currentUser.username) {
            this.onEdit(lastMessage.id);
        }
    }

    onEdit(id) {
        this.props.history.push(`/message/${id}`);
    }

    onAddMessage() {
        this.props.history.push('/message');
    }

    render() {
        const {
            currentUser,
            messageData,
            addMessage,
            deleteMessage
        } = this.props;

        const { messages, loading, error } = messageData;
        const { username, avatar } = currentUser;

        const sortedMessages = this.messageSort(messages);

        return (
            <div className="chat-container">
                {error && <Error message={error}/>}
                {loading && <Spinner/>}
                {messages.length && (
                    <>
                        <ChatHeader
                            chatName="My Chat"
                            userCount={this.countUsers()}
                            messageCount={sortedMessages.length}
                            lastMessageTime={this.getLastMessageTime()}
                        />
                        <MessageList
                        currentUser={currentUser.username}
                        messages={sortedMessages}
                        onMessageEdit={this.onEdit}
                        onMessageDelete={deleteMessage}
                        />
                        <MessageInput onSubmit={_.partial(addMessage, username, avatar)} placeholder="Enter message" buttonText="Send"/>
                        <button className="huge primary ui button fixed" type="button" onClick={this.onAddMessage}>
                            Add Message
                        </button>
                    </>
                )}
            </div>
        );
    }
}

Chat.propTypes = {
    currentUser: PropTypes.exact({
        username: PropTypes.string.isRequired,
        avatar: PropTypes.string,
        isAdmin: PropTypes.bool.isRequired
    }),
    messageData: PropTypes.exact({
        messages: PropTypes.arrayOf(
            PropTypes.exact({
                id: PropTypes.number.isRequired,
                user: PropTypes.string.isRequired,
                avatar: PropTypes.string.isRequired,
                created_at: PropTypes.string.isRequired,
                message: PropTypes.string.isRequired,
                marked_read: PropTypes.bool.isRequired
            })
        ),
        loading: PropTypes.bool.isRequired,
        error: PropTypes.string
    }).isRequired,
    fetchMessages: PropTypes.func.isRequired,
    addMessage: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    currentUser: state.currentUser,
    messageData: state.messageData
});

const mapDispatchToProps = {
    fetchMessages,
    addMessage,
    deleteMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);