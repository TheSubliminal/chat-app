import { fetchMessages } from '../../routines/routines';

const initialState = {
    messages: [],
    loading: false,
    error: null
};

const messageData = (state = initialState, action) => {
    switch (action.type) {
        case fetchMessages.TRIGGER:
            return {
                ...state,
                loading: true
            };
        case fetchMessages.SUCCESS:
            return {
                ...state,
                messages: action.payload
            };
        case fetchMessages.FAILURE:
            return {
                ...state,
                error: action.payload
            };
        case fetchMessages.FULFILL:
            return {
                ...state,
                loading: false
            };
        default:
            return state;
    }
};

export default messageData;