import { all } from 'redux-saga/effects';

import loginSaga from './LoginPage/sagas';
import messagesSaga from './Chat/sagas';
import messageSaga from './MessageEditor/sagas';
import usersSagas from './UserList/sagas';
import userSaga from './UserEditor/sagas';

export default function* rootSaga() {
    yield all([
        loginSaga(),
        messagesSaga(),
        messageSaga(),
        usersSagas(),
        userSaga()
    ]);
};