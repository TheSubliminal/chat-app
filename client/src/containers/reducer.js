import { combineReducers } from 'redux';

import currentUser from './App/reducer';
import messageData from './Chat/reducer';
import loginData from './LoginPage/reducer';
import editedMessage from './MessageEditor/reducer';
import userData from './UserList/reducer';
import editedUser from './UserEditor/reducer';

export default combineReducers({
    currentUser,
    messageData,
    loginData,
    editedMessage,
    userData,
    editedUser
});