import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ currentUser, adminPath, component: Component, ...rest }) => {
    return (
        <Route {...rest} render={props => {
            if (currentUser) {
                if (!adminPath) {
                    return <Component {...props}/>;
                } else if (!currentUser.isAdmin) {
                    return <Redirect to="/"/>;
                } else {
                    return <Component {...props}/>;
                }
            } else {
                return <Redirect to="/login"/>
            }
        }}/>
    );
};

PrivateRoute.propTypes = {
    currentUser: PropTypes.exact({
        username: PropTypes.string.isRequired,
        avatar: PropTypes.string,
        isAdmin: PropTypes.bool.isRequired
    }),
    adminPath: PropTypes.bool,
    component: PropTypes.any
};

const mapStateToProps = state => ({
    currentUser: state.currentUser
});

export default connect(mapStateToProps)(PrivateRoute);