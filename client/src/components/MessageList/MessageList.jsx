import React from 'react';
import PropTypes from 'prop-types';
import Message from '../Message/Message';
import MessageDivider from '../MessageDivider/MessageDivider';

import './message-list.css';

const MS_IN_DAY = 1000 * 60 * 60 * 24;

const MessageList = ({ currentUser, messages, onMessageEdit, onMessageDelete }) => {
    const getDateDiff = (dateStringA, dateStringB) => {
        const dateA = new Date(dateStringA);
        const dateB = new Date(dateStringB);
        const currentDate = new Date();
        currentDate.setHours(0, 0, 0, 0);

        const dayDiff = dateA.getDate() - dateB.getDate();
        const monthDiff = dateA.getMonth() - dateB.getMonth();
        const yearDiff = dateA.getFullYear() - dateB.getFullYear();

        const datePicker = (diff, dateA, dateB) => {
            if (diff > 0) {
                return dateB.toLocaleDateString();
            } else {
                return dateA.toLocaleDateString();
            }
        };

        if (dayDiff) {
            const pickedDate = datePicker(dayDiff, dateA, dateB);
            if (currentDate - new Date(pickedDate) === MS_IN_DAY) {
                return 'Yesterday';
            }
            return pickedDate
        } else if (monthDiff) {
            return datePicker(monthDiff, dateA, dateB);
        } else if (yearDiff) {
            return datePicker(yearDiff, dateA, dateB);
        }
    };

    return (
        <div className="message-list">
            {messages.map((message, index, messages) => {
                let divider;

                if (index > 0) {
                    const dateDiff = getDateDiff(message.created_at, messages[index - 1].created_at);
                    if (dateDiff) {
                        divider = (
                            <MessageDivider>
                                {dateDiff}
                            </MessageDivider>
                        );
                    }
                }

                return (
                    <React.Fragment key={message.id}>
                        {divider}
                        <Message
                            message={message}
                            isCurrentUserMessage={currentUser === message.user}
                            onMessageEdit={onMessageEdit}
                            onMessageDelete={onMessageDelete}
                        />
                    </React.Fragment>
                );
            })}
        </div>
    );
};

MessageList.propTypes = {
    messages: PropTypes.arrayOf(
        PropTypes.exact({
            id: PropTypes.number.isRequired,
            user: PropTypes.string.isRequired,
            avatar: PropTypes.string.isRequired,
            created_at: PropTypes.string.isRequired,
            message: PropTypes.string.isRequired,
            marked_read: PropTypes.bool.isRequired
        })
    ).isRequired,
    onMessageEdit: PropTypes.func.isRequired,
    onMessageDelete: PropTypes.func.isRequired
};

export default MessageList;