import React from 'react';

import './not-found.css';

const NotFound = () => {
    return (
        <div className="not-found-container">
            <div className="not-found-message">
                Page Not Found!
            </div>
        </div>
    );
};

export default NotFound;
