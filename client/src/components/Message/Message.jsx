import React from 'react';
import PropTypes from 'prop-types';

import './message.css';
import 'semantic-ui-comment/comment.css';
import 'semantic-ui-button/button.css';

class Message extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isHovered: false,
            isLikeActive: false,
            liked: props.message.liked || false
        };

        this.toggleProp = this.toggleProp.bind(this);
    }

    toggleProp(prop) {
        this.setState(state => ({
            [prop]: !state[prop]
        }))
    }

    render() {
        const { isCurrentUserMessage, onMessageDelete, onMessageEdit } = this.props;
        const { id, user, avatar, created_at, message, marked_read } = this.props.message;
        const { isHovered, isLikeActive, liked } = this.state;

        let actions = null;
        if (!isCurrentUserMessage) {
            actions = (
                <div className="actions">
                    <i
                        className={`${isLikeActive || liked ? 'fas' : 'far'} fa-heart ${liked ? 'liked' : ''}`}
                        onMouseEnter={() => this.toggleProp('isLikeActive')}
                        onMouseLeave={() => this.toggleProp('isLikeActive')}
                        onClick={() => this.toggleProp('liked')}
                    />
                </div>
            );
        } else if (isHovered) {
            actions = (
                <div className="actions">
                    <a className="message-control-button" onClick={() => onMessageEdit(id)}>Edit</a>
                    <a className="message-control-button" onClick={() => onMessageDelete(id)}>Delete</a>
                </div>
            );
        }

        return (
            <div
                className={`ui comments ${isCurrentUserMessage ? 'current-user-message' : ''}`}
                onMouseEnter={() => this.toggleProp('isHovered')}
                onMouseLeave={() => this.toggleProp('isHovered')}
            >
                <div className="comment">
                    {
                        !isCurrentUserMessage &&
                        <a className="avatar">
                            <img src={avatar} alt="Avatar"/>
                        </a>
                    }
                    <div className="content">
                        <a className="author">{user}</a>
                        <div className="metadata">
                            <span className="date">{created_at}</span>
                            {marked_read ? <i className="fas fa-check-double"/> : <i className="fas fa-check"/>}
                        </div>
                        <div className="text">{message}</div>
                        {actions}
                    </div>
                </div>
            </div>
        );
    }
}

Message.propTypes = {
    message: PropTypes.shape({
        id: PropTypes.number,
        user: PropTypes.string,
        avatar: PropTypes.string,
        created_at: PropTypes.string,
        message: PropTypes.string,
        marked_read: PropTypes.bool
    }).isRequired,
    isCurrentUserMessage: PropTypes.bool.isRequired,
    onMessageEdit: PropTypes.func.isRequired,
    onMessageDelete: PropTypes.func.isRequired
};

export default Message;