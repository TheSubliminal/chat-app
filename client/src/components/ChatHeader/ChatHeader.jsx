import React from 'react';
import PropTypes from 'prop-types';

import './chat-header.css'

const ChatHeader = ({ chatName, userCount, messageCount, lastMessageTime }) => {
    return (
        <header className="chat-header">
            <div className="chat-name">{chatName}</div>
            <div className="user-count">{userCount} participants</div>
            <div className="message-count">{messageCount} messages</div>
            <div className="last-message">last message {lastMessageTime}</div>
        </header>
    );
};

ChatHeader.propTypes = {
    chatName: PropTypes.string.isRequired,
    userCount: PropTypes.number.isRequired,
    messageCount: PropTypes.number.isRequired,
    lastMessageTime: PropTypes.string.isRequired
};

export default ChatHeader;