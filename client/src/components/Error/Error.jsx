import React from 'react';
import PropTypes from 'prop-types';

import 'semantic-ui-message/message.css';
import './error.css';

const Error = ({ message }) => {
    return (
        <div className="ui negative message fixed">
            <div className="header">
                {message}
            </div>
        </div>
    );
};

Error.propTypes = {
    message: PropTypes.string.isRequired
};

export default Error;
