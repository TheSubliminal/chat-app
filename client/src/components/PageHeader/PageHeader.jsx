import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import './page-header.css';

class PageHeader extends React.PureComponent {
    render() {
        const { currentUser } = this.props;

        return (
            <header className="page-header">
                <div className="logo">
                    <img src="https://image.flaticon.com/icons/svg/1078/1078011.svg" alt="Logo"/>
                    <span>Chat App</span>
                </div>
                {currentUser && (
                    <nav>
                        <NavLink exact to="/" activeClassName="matching-link">Chat</NavLink>
                        {currentUser.isAdmin && <NavLink to="/users" activeClassName="matching-link">User List</NavLink>}
                    </nav>
                )}
            </header>
        );
    }
}

const mapStateToProps = state => ({
    currentUser: state.currentUser
});

export default connect(mapStateToProps)(PageHeader);