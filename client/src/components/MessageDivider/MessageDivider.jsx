import React from 'react';

import 'semantic-ui-divider/divider.css';
import './divider.css';

const MessageDivider = (props) => {
    return (
        <h4 className="ui horizontal inverted divider">
            {props.children}
        </h4>
    );
};

export default MessageDivider;