import React from 'react';
import PropTypes from 'prop-types';

import 'semantic-ui-button/button.css';
import './user-item.css';

const UserItem = ({ username, avatar, onEdit, onDelete }) => {
    return (
        <div className="user-item">
            <div className="user-info">
                {avatar && <img src={avatar} alt="Avatar" className="avatar"/>}
                <span className="username">{username}</span>
            </div>
            <div className="controls">
                <button className="big primary ui button" type="button" onClick={onEdit}>Edit</button>
                <button className="big red ui button" type="button" onClick={onDelete}>Delete</button>
            </div>
        </div>
    );
};

UserItem.propTypes = {
    username: PropTypes.string.isRequired,
    avatar: PropTypes.string,
    onEdit: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired
};

export default UserItem;