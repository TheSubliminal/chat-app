import React from 'react';
import PropTypes from 'prop-types';

import 'semantic-ui-input/input.css';
import 'semantic-ui-button/button.css';
import './message-input.css';

class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messageText: this.props.initialValue,
            isValid: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();

        this.props.onSubmit(this.state.messageText.trim());

        this.setState({
            messageText: ''
        });

        const scrollingElement = (document.scrollingElement || document.body);
        scrollingElement.scrollTop = scrollingElement.scrollHeight;
    }

    onChange(event) {
        const value = event.target.value;

        this.setState({
            messageText: value
        });

        this.validate();
    }

    validate() {
        this.setState(state => ({
            isValid: !!state.messageText.trim()
        }));
    }

    render() {
        const { messageText, isValid } = this.state;
        const { placeholder, buttonText } = this.props;

        return (
            <form onSubmit={this.handleSubmit}>
                <div className="ui big action input">
                    <input value={messageText} type="text" placeholder={placeholder} onChange={this.onChange}/>
                    <button className="big ui button blue" type="submit" disabled={!isValid}>{buttonText}</button>
                </div>
            </form>
        );
    }
}

MessageInput.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    initialValue: PropTypes.string,
    placeholder: PropTypes.string.isRequired,
    buttonText: PropTypes.string.isRequired
};

MessageInput.defaultProps = {
    initialValue: ''
};

export default MessageInput;