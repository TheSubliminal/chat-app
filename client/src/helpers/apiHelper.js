const API_BASE_URL = 'http://localhost:3000';

const getApiUrl = (path) => `${API_BASE_URL}${path}`;

export const callApi = (path, method, body) => {
    const requestPath = getApiUrl(path);

    const requestArgs = {
        method,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        ...(method === 'GET' ? {} : { body: JSON.stringify(body) })
    };

    return fetch(requestPath, requestArgs);
};